# Colors.
unset LSCOLORS
export CLICOLOR=1
export CLICOLOR_FORCE=1
export HELM_DIFF_COLOR=1

# Don't require escaping globbing characters in zsh.
unsetopt nomatch

# Nicer prompt.
export PS1=$'\n'"%F{green} %*%F %3~ %F{white}"$'\n'"$ "

# Enable plugins.
plugins=(git brew history kubectl history-substring-search)

# Custom $PATH with extra locations.
CUSTOM_PATH=/opt/homebrew/bin
CUSTOM_PATH=${CUSTOM_PATH}:/opt/homebrew/sbin
CUSTOM_PATH=${CUSTOM_PATH}:/usr/local/bin
CUSTOM_PATH=${CUSTOM_PATH}:/usr/local/sbin
CUSTOM_PATH=${CUSTOM_PATH}:${HOME}/bin
CUSTOM_PATH=${CUSTOM_PATH}:${HOME}/go/bin
CUSTOM_PATH=${CUSTOM_PATH}:/usr/local/git/bin
CUSTOM_PATH=${CUSTOM_PATH}:${HOME}/.docker/bin
CUSTOM_PATH=${CUSTOM_PATH}:${HOME}/.composer/vendor/bin
export PATH=${CUSTOM_PATH}:${PATH}

# Bash-style time output.
export TIMEFMT=$'\nreal\t%*E\nuser\t%*U\nsys\t%*S'

# https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html
export GPG_TTY=$(tty)

# Set architecture-specific brew share path.
arch_name="$(uname -m)"
if [ "${arch_name}" = "x86_64" ]; then
    share_path="/usr/local/share"
elif [ "${arch_name}" = "arm64" ]; then
    share_path="/opt/homebrew/share"
else
    echo "Unknown architecture: ${arch_name}"
fi

# Allow history search via up/down keys.
source ${share_path}/zsh-history-substring-search/zsh-history-substring-search.zsh
bindkey "^[[A" history-substring-search-up
bindkey "^[[B" history-substring-search-down

# Completions.
autoload -Uz compinit && compinit
# Case insensitive.
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*'

# Tell homebrew to not autoupdate every single time I run it (just once a week).
export HOMEBREW_AUTO_UPDATE_SECS=604800
# direnv hook
eval "$(direnv hook zsh)"

# Allow Composer to use almost as much RAM as Chrome.
export COMPOSER_MEMORY_LIMIT=-1

# 1Password Shell plugins
source $HOME/.config/op/plugins.sh
