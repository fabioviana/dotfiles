# fabioviana's dotfiles

Fork from [Mac Development Ansible Playbook](https://github.com/geerlingguy/mac-dev-playbook)

## Local project development

Need to configure origin push URL to use SSH to commit changes:

```bash
git remote set-url origin git@gitlab.com:fabioviana/dotfiles.git --push
```
